package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private final String DISTANCIEL= getResources().getStringArray(R.array.list_salles)[0];
    private String salle=DISTANCIEL;
    private String poste="";
    // TODO Q2.c
    SuiviViewModel mod;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        // TODO Q2.c
        this.mod=new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView username = (TextView) view.findViewById(R.id.tvLogin);
            mod.setUsername(username.getText()+"");

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });




        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    // TODO Q9
}